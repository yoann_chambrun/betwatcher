DROP DATABASE IF EXISTS `betwatcher`;
CREATE DATABASE `betwatcher`
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_unicode_ci;
USE `betwatcher`;

CREATE TABLE `user`
(
    `user_id`       INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `username`      VARCHAR(255) DEFAULT NULL UNIQUE,
    `email`         VARCHAR(255) DEFAULT NULL UNIQUE,
    `display_name`  VARCHAR(50) DEFAULT NULL,
    `password`      VARCHAR(128) NOT NULL,
    `state`         SMALLINT UNSIGNED,
    `role`          VARCHAR(10) NOT NULL,
    `avatar`        VARCHAR(100) DEFAULT NULL
) ENGINE=InnoDB CHARSET="utf8";