<?php

return [
    'service_manager' => [
        'invokable' => [
            Twig_Extensions_Extension_I18n::class => Twig_Extensions_Extension_I18n::class,
        ],
    ],
    'zend_twig'       => [
        'extensions' => [
            Twig_Extensions_Extension_I18n::class,
        ],
    ],
];